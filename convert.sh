#!/bin/bash
find .  -name \*.html | \
  (while read file; do
    if [[ "$file" != *.DS_Store* ]]; then
      if [[ "$file" != *-utf8* ]]; then
        echo "convering ${file}"
        iconv -f CP1251 -t UTF-8 "$file" > "$file-utf8";
        rm $file; mv "$file-utf8" "$file";
      fi
    fi
  done);
